<?php

require_once __DIR__ . '/ip_in_range.php';

class BitbucketDeployer
{
	private $targets;

	/**
	 * @param array $targets Deploy targets, e.g.:
	 *     [
	 *         'user/repository' => '/path/to/project/directory',
	 *         'user/repo2#staging' => '/path/to/staging/directory'
	 *     ]
	 */
	public function __construct($targets)
	{
		$this->targets = $targets;
	}

	/**
	 * Attempts to match the payload to a deploy target
	 * If there's a match, pulls the latest changes to the target directory with git
	 * @param  object  $payload                Parsed bitbucket post hook payload
	 * @param  boolean $createLatestCommitFile Whether to create a .latest-commit file with the latest commit's hash in the target directory after deployment
	 * @return string                          If the deployment is successful, returns the deployed repository's name#branch
	 */
	public function deploy($payload, $createLatestCommitFile = false)
	{
		$repository = trim($payload->repository->full_name, '/');
		$repositoryWithBranch = $repository . '#' . $payload->push->changes[0]->new->name;

		$targets = [];

		if (isset($this->targets[$repository])) {
			$targets[] = $this->targets[$repository];
		}

		if (isset($this->targets[$repositoryWithBranch])) {
			$targets[] = $this->targets[$repository];
		}

		// If the target is not found, throw an exception
		if (count($targets) === 0) {
			throw new \Exception('No matching targets found for ' . $repository . ' or ' . $repositoryWithBranch);
		}

		foreach ($targets as $target) {
			// Change the working directory to the target
			chdir($target);

			// Clean the repo and pull latest changes
			$commands = array(
				'echo $PWD',
				'whoami',
				'git reset --hard HEAD',
				'git pull',
				'git status',
				'git submodule sync',
				'git submodule update',
				'git submodule status',
			);

			if ($createLatestCommitFile) {
				$commands[] = 'git rev-parse HEAD > .latest-commit && git show -s --format="%ci" HEAD >> .latest-commit';
			}

			foreach ($commands as $command) shell_exec($command);
		}

		return $repositoryWithBranch;
	}
}
