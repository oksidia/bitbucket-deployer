<?php

date_default_timezone_set('UTC');

require_once __DIR__ . '/../BitbucketDeployer.php';
require_once __DIR__ . '/../ip_in_range.php';

$logPath = __DIR__ . '/../deploy.log';
$log = function($path, $str) {
	$message = date('[Y-m-d H:i:s] ') . $str . "\n";
	file_put_contents($path, $message, FILE_APPEND);
};

// Only POST requests
if ($_SERVER['REQUEST_METHOD'] !== 'POST') exit;

// Limit allowed requests to Bitbucket's IP ranges
$allowedIPs = include(__DIR__ . '/../bitbucketIPs.php');

$clientIPIsAllowed = false;
foreach ($allowedIPs as $ip) {
	if (ip_in_range($_SERVER['REMOTE_ADDR'], $ip)) {
		$clientIPIsAllowed = true;
		break;
	}
}

if (! $clientIPIsAllowed) {
	$log($logPath, 'Request from a disallowed IP address: ' . $_SERVER['REMOTE_ADDR']);
	exit;
}

// Load deploy targets and initialize deployer
$targets = include(__DIR__ . '/../targets.php');
$deployer = new BitbucketDeployer($targets);

// Parse the JSON payload
// https://confluence.atlassian.com/bitbucket/event-payloads-740262817.html#EventPayloads-Push
$payload = json_decode(file_get_contents("php://input"));

// Attempt deployment
try {
	$repositoryWithBranch = $deployer->deploy($payload, true);
} catch (\Exception $exception) {
	$log($logPath, $exception->getMessage());
	exit;
}

// Success!
$log($logPath, 'Successfully deployed ' . $repositoryWithBranch);
